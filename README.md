## Generalized GPU Canny Edge Detection ##

![example](img/0000.png)  
Top left: original image
Top right : final edges
Bottom left: normalized edge gradient
Bottom right: edge orientation encoded in [0->360] encoded in [0->1]  
  
  
This project implements a templated GPU [Canny Edge Detector](https://en.wikipedia.org/wiki/Canny_edge_detector), giving complete control over all the stages of the process : gaussian blur -> gradient and angle -> non maximal suppression -> hysterezis thresholding -> edge thinning. 

Stages:  
- The blur is implemented [with a linearly sampled separable Gaussian Blur](https://bitbucket.org/lucianpetrescu/fast_gpu_blur), and both the sigma and the kernel size are controlled by the user.    
- The gradient and angle can be computed with any of the following kernels: [Roberts Cross 2x2](https://en.wikipedia.org/wiki/Roberts_cross), [Sobel 3x3, Sobel 5x5](https://en.wikipedia.org/wiki/Sobel_operator), [Scharr 3x3](https://en.wikipedia.org/wiki/Sobel_operator#Alternative_operators), [Prewitt 3x3, Prewitt 5x5](https://en.wikipedia.org/wiki/Prewitt_operator), [Kirsch3x3, Kirsch5x5](https://en.wikipedia.org/wiki/Kirsch_operator), [FreiChen3x3](http://www.cs.tau.ac.il/~turkel/notes/edge1b.pdf), Line3x3, [Log3x3, Log5x5](http://mccormickml.com/2013/02/27/laplacian-of-gaussian-marr-hildreth-edge-detector/).  
- The non-maximum suppression stage is optional.  
- The edge tracking by hysterezis is implemented as an iterative processes and the number of iterations is controlled by the user.  
- The edge thinning stage uses the [Zhang Suen algorithm](https://rosettacode.org/wiki/Zhang-Suen_thinning_algorithm) and it is implemented as an iterative processes, the number of iterations is controlled by the user.

The edge detector can also be used to compute flux (the gradient clamped and normalized to the upper threshold)

SPEED : about 2ms for a 512x512 image, using default parameters, measured on a NVIDIA GTX 960M laptop card).

NOTE: OpenGL 4.5 compatible hardware is required to run this project.
 
#### HOW TO USE IN OTHER PROJECTS? ####
Include the files, and modify the next line: 

	// module needs OpenGL and wic provides it
	#include "../dependencies/lap_wic/lap_wic.hpp"

with a header which includes OpenGL functions. Then it's as simple as this:

	GPUEdgeDetector edge_detector; 
	edge_detector.setInput(GL_TEXTURE, NUM_CHANNELS);

	//compute binarized edges
	edge_detector.computeEdge(gaussian_sigma, gaussian_kernel_size, edge_operator, threshold_upper, threshold_lower, non_maximum_suppression?, hysterezis_iterations, options.thinning_iterations);

	//or compute flux
	edge_detector.computeFlux(options.gaussianblur_kernel_sigma, (unsigned int)options.gaussianblur_kernel_size, options.edge_operator, options.threshold_upper);

	GL_TEXTURE = getOutputMagnitudeGL();
	GL_TEXTURE = getOutputAngleGL();

#### CONTROLS ####

The application does not have a GUI in order to minimize code. The application is controlled through the keyboard. Here is a screenshot of the console with the output for help (H key):

![help](img/help.png)

### ALL SCREENSHOTS ###

Original image  
![](img/inout.png)  
Edge angles  
![](img/angles.png)  
 ---
Edges with Roberts Cross  
![](img/ed_roberts.png)    
Edges with Frei Chen  
![](img/ed_freichen.png)   
Edges with Kirsch 3x3  
![](img/ed_kirsch.png)   
Edges with Prewitt 3x3  
![](img/ed_prewitt.png)     
Edge with Scharr 3x3  
![](img/ed_scharr.png)  
Edges with Sobel 3x3   
![](img/ed_sobel3.png)  
Edges with Sobel 5x5   
![](img/ed_sobel5.png) 
 ---   
Edges without histerezis tracking  
![](img/histerezis_no.png)    
Edges with one iteration of histerezis tracking   
![](img/histerezis_1it.png)
 ---  
Edges without thinning  
![](img/thin_no.png)  
Edges with 3 iterations of Zhang Suen thinning  
![](img/thin_3it.png)  

#### BUILDING and DEPENDENCIES ####

This project uses [lap_wic](https://bitbucket.org/lucianpetrescu/public) for all OpenGL context, input handling and windowing needs and [glm](http://glm.g-truc.net/0.9.8/index.html) for mathematics. 
The project should compile with any CPP11 compiler. A 2015 visual studio project is prodived in the /vstudio folder. makefiles for visual studio and make+GCC can be found in the root folder. This project requires an OpenGL 4.5 graphics card with up-to-date drivers 

Tested under Windows (vstudio and gcc with mingw). Should work under Unix / MacOS. 

#### LICENSE ####

The MIT License (MIT)
 
Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.