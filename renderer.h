///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
#include "core/gpu_buffer.h"
#include "core/gpu_texture.h"
#include "core/gpu_shader.h"

namespace edge{
	
	class Renderer{
	public:
		Renderer();
		Renderer(const Renderer&) = delete;
		Renderer(Renderer&&) = delete;
		Renderer& operator=(const Renderer&) = delete;
		Renderer& operator=(Renderer&&) = delete;
		~Renderer();

		//output and control
		void setInput(const GPUTexture2D* input);
		void setInputFilteringNearest();
		void setInputFilteringBilinear();
		void setOutputPath(const std::string& path);
		void screenshot(const std::string& filename);

		//render related
		void render();
		void resize(unsigned int width, unsigned int height);

	private:
		//fullscreen
		GLuint fs_vbo = 0, fs_vao = 0;
		GPUShader fs_shader;
		GPUTexture2D fs_texture_noinput;
		GPUTextureSampler fs_sampler;
		unsigned int fs_width = 0, fs_height = 0;

		//state
		const GPUTexture2D* input;

		//output path
		std::string output_path = "";
	};
}
