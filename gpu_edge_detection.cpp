///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "gpu_edge_detection.h"

namespace edge{
	GPUEdgeDetector::GPUEdgeDetector() {
		//samplers
		sampler_near.setClamp();
		sampler_near.setNearest();
		sampler_bilinear.setClamp();
		sampler_bilinear.setBilinear();

		//gaussian blurring
		shader_blur.load("../shaders/edge_gaussian_blur.comp");
		shader_edge.load("../shaders/edge_detection.comp");
		gpu_tex1.create(GL_R8, 10, 10, false);
		gpu_tex2.create(GL_R8, 10, 10, false);
		gpu_tex3.create(GL_R8, 10, 10, false);
	}
	GPUEdgeDetector::~GPUEdgeDetector() {
	}

	void GPUEdgeDetector::setInput(GLuint texid, int num_channels) {
		int w, h;
		glGetTextureLevelParameteriv(texid, 0, GL_TEXTURE_WIDTH, &w);
		glGetTextureLevelParameteriv(texid, 0, GL_TEXTURE_HEIGHT, &h);
		input.num_channels = num_channels;
		input.width = w;
		input.height = h;
		input.texid = texid;
	}
	void GPUEdgeDetector::setInput(GPUTexture2D* in_input, int in_num_channels) {
		//input
		input.num_channels = in_num_channels;
		input.width = in_input->getWidth();
		input.height = in_input->getHeight();
		input.texid = in_input->getGLobject();

		//resize?
		if (input.width != gpu_tex1.getWidth() || input.height != gpu_tex1.getHeight()) {
			gpu_tex1.create(GL_R8, input.width, input.height, false);
			gpu_tex2.create(GL_R8, input.width, input.height, false);
			gpu_tex3.create(GL_R8, input.width, input.height, false);
		}
	}
	void GPUEdgeDetector::computeEdge(float blur_kernel_sigma, unsigned int blur_kernel_size, Operator edge_op, float threshold_upper, float threshold_lower, bool non_maximum_suppression, unsigned int hysterezis_iterations, unsigned int thinning_iterations){
		unsigned int w = input.width;
		unsigned int h = input.height;

		//input will always be on zero
		glBindTextureUnit(0, input.texid);

		//grayscale
		sampler_near.bind(0);
		shader_blur.bind();
		shader_blur.setUniform("CHANNELS", (int)input.num_channels);
		shader_blur.setUniform("PASS", 0);
		gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
		glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		//blur
		if (blur_kernel_size > 1) {
			sampler_bilinear.bind(0);
			shader_blur.setUniform("SIGMA", blur_kernel_sigma);
			shader_blur.setUniform("SIZE", (int)(blur_kernel_size / 2));

			shader_blur.setUniform("PASS", 1);
			gpu_tex1.bindAsTexture(0);
			gpu_tex3.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);

			shader_blur.setUniform("PASS", 2);
			gpu_tex3.bindAsTexture(0);
			gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}

		//edge detect gradient
		sampler_near.bind(0);
		sampler_near.bind(1);
		shader_edge.bind();
		switch (edge_op) {
		case Operator::RobertsCross2x2:
			shader_edge.setSubroutine("operatorRobertsCross2x2", GL_COMPUTE_SHADER);
			break;
		case Operator::Sobel3x3:
			shader_edge.setSubroutine("operatorSobel3x3", GL_COMPUTE_SHADER);
			break;
		case Operator::Sobel5x5:
			shader_edge.setSubroutine("operatorSobel5x5", GL_COMPUTE_SHADER);
			break;
		case Operator::Scharr3x3:
			shader_edge.setSubroutine("operatorScharr3x3", GL_COMPUTE_SHADER);
			break;
		case Operator::Prewitt3x3:
			shader_edge.setSubroutine("operatorPrewitt3x3", GL_COMPUTE_SHADER);
			break;
		case Operator::Prewitt5x5:
			shader_edge.setSubroutine("operatorPrewitt5x5", GL_COMPUTE_SHADER);
			break;
		case Operator::Kirsch3x3:
			shader_edge.setSubroutine("operatorKirsch3x3", GL_COMPUTE_SHADER);
			break;
		case Operator::Kirsch5x5:
			shader_edge.setSubroutine("operatorKirsch5x5", GL_COMPUTE_SHADER);
			break;
		case Operator::FreiChen3x3:
			shader_edge.setSubroutine("operatorFreiChen3x3", GL_COMPUTE_SHADER);
			break;
		case Operator::Line3x3:
			shader_edge.setSubroutine("operatorLine3x3", GL_COMPUTE_SHADER);
			break;
		case Operator::Log3x3:
			shader_edge.setSubroutine("operatorLog3x3", GL_COMPUTE_SHADER);
			break;
		case Operator::Log5x5:
			shader_edge.setSubroutine("operatorLog5x5", GL_COMPUTE_SHADER);
			break;
		default:
			break;
		}
		shader_edge.setUniform("THRESHOLD_UPPER", threshold_upper);
		shader_edge.setUniform("THRESHOLD_LOWER", threshold_lower);
		shader_edge.setUniform("PASS", 1);
		gpu_tex1.bindAsTexture(0);
		gpu_tex3.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
		gpu_tex2.bindAsImage(1, GL_WRITE_ONLY, 0, GL_R8);
		glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		//non maximal suppression
		if (non_maximum_suppression) {
			shader_edge.setUniform("PASS", 2);
			gpu_tex3.bindAsTexture(0);
			gpu_tex2.bindAsTexture(1);
			gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}
		else {
			gpu_tex3.swap(gpu_tex1);
		}

		//hysteresis thresholding
		for (int i = 0; i < (int)hysterezis_iterations; i++) {
			shader_edge.setUniform("PASS", 3);
			gpu_tex1.bindAsTexture(0);
			gpu_tex3.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
			gpu_tex3.bindAsTexture(0);
			gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}

		//thinning iterations
		for (int i = 0; i < (int)thinning_iterations; i++) {
			shader_edge.setUniform("PASS", 4);
			shader_edge.setUniform("THINNING_ITERATION", 0);
			gpu_tex1.bindAsTexture(0);
			gpu_tex3.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
			shader_edge.setUniform("THINNING_ITERATION", 1);
			gpu_tex3.bindAsTexture(0);
			gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_R8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}
	}
	void GPUEdgeDetector::computeFlux(float blur_kernel_sigma, unsigned int blur_kernel_size, Operator edge_op, float threshold_upper){
		computeEdge(blur_kernel_sigma, blur_kernel_size, edge_op, threshold_upper, 0, false, 0, 0);
	}
	GPUTexture2D* GPUEdgeDetector::getOutputMagnitude() {
		return &gpu_tex1;
	}
	GPUTexture2D* GPUEdgeDetector::getOutputAngle() {
		return &gpu_tex2;
	}
	GLuint GPUEdgeDetector::getOutputMagnitudeGL() {
		return gpu_tex1.getGLobject();
	}
	GLuint GPUEdgeDetector::getOutputAngleGL() {
		return gpu_tex2.getGLobject();
	}
	void GPUEdgeDetector::debugReloadShaders() {
		shader_blur.reload();
		shader_edge.reload();
	}
	const std::string GPUEdgeDetector::getOperatorName(Operator edge_op) {
		switch (edge_op) {
		case Operator::RobertsCross2x2:
			return "RobertsCross2x2";
			break;
		case Operator::Sobel3x3:
			return "Sobel3x3";
			break;
		case Operator::Sobel5x5:
			return "Sobel5x5";
			break;
		case Operator::Scharr3x3:
			return "Scharr3x3";
			break;
		case Operator::Prewitt3x3:
			return "Prewitt3x3";
			break;
		case Operator::Prewitt5x5:
			return "Prewitt5x5";
			break;
		case Operator::Kirsch3x3:
			return "Kirsch3x3";
			break;
		case Operator::Kirsch5x5:
			return "Kirsch5x5";
			break;
		case Operator::FreiChen3x3:
			return "FreiChen3x3";
			break;
		case Operator::Line3x3:
			return "Line3x3";
			break;
		case Operator::Log3x3:
			return "Log3x3";
			break;
		case Operator::Log5x5:
			return "Log5x5";
			break;
		default:
			break;
		}
		return "";
	}

}
