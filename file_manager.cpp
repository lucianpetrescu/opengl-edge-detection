///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "file_manager.h"
#include "dependencies/tinydir/tinydir.h"
#include <iostream>
#include <vector>

namespace edge{
	
	FileManager::FileManager() {
		asset_path = "";
	}
	FileManager::FileManager(const std::string& in_asset_path) {
		asset_path = in_asset_path;
	}

	const std::string FileManager::internalNextFile(const std::string& fromfile, bool next) {
		//while this is inefficient in comparison to loading the assets once and just cycling the list it can
		//handle adding and erasing files on the fly, thus it is preferred. Furthermore this op cost <<<<<<<
		//cost of actually loading the file.

		//discover files
		std::vector<std::string> contents;
		int fromfile_index = -1;
		tinydir_dir dir;
		tinydir_open_sorted(&dir, asset_path.c_str());
		for (size_t i = 0; i < dir.n_files; i++)
		{
			tinydir_file file;
			tinydir_readfile_n(&dir, &file, i);
			if (!file.is_dir) {
				std::string ext(file.extension);
				//targa, png, jpg/jpeg, bmp, gif
				if (ext == "png" || ext == "jpeg" || ext == "jpg" || ext == "tga" || ext == "bmp" || ext == "gif") {
					contents.emplace_back(file.name);
					if (fromfile == asset_path + file.name) fromfile_index = (int)contents.size() - 1;
				}	
			}
		}

		//nothing here
		if (contents.size() == 0) return "";

		//next
		if (next) {
			if (fromfile == "") return contents.front();	//first from start if relative search starts from nothing
			fromfile_index = (fromfile_index + 1 + (int)contents.size()) % (int)contents.size();
			return contents[fromfile_index];
		}
		//prev
		else {
			if (fromfile == "") return contents.back();	//first from end if relative search starts from nothing
			fromfile_index = (fromfile_index - 1 + (int)contents.size()) % (int)contents.size();
			return contents[fromfile_index];
		}
	}

	void FileManager::internalLoad(const std::string& file) {
		bool ret;
		image.load(output.filename, lap::Image::ColorSpace::SRGB, &ret);

		//no Y flipping, renderer is aware of this
		unsigned int channels = image.getNumChannels();
		GLenum internal_format = GL_RGB8, data_format = GL_RGB;
		if (channels == 1) {
			internal_format = GL_R8;
			data_format = GL_RED;
		}
		else if (channels == 2) {
			internal_format = GL_RG8;
			data_format = GL_RG;
		}
		else if (channels == 3) {
			internal_format = GL_RGB8;
			data_format = GL_RGB;
		}
		else if (channels == 4) {
			internal_format = GL_RGBA8;
			data_format = GL_RGBA;
		}
		if (ret) {
			std::cout << " --- [FileManager] using image from " << output.filename << std::endl;
			texture.create(internal_format, image.getWidth(), image.getHeight(), false, -1);
			texture.update(data_format, GL_UNSIGNED_BYTE, image.getData(), false);
		}else{
			std::cout << " --- [FileManager] ERROR when reading from " << output.filename << std::endl;
			texture.create(GL_R8, 4, 4, false, 0);	//avoid GL crashes
			texture.update(data_format, GL_UNSIGNED_BYTE, nullptr, false);
		}

		output.width = image.getWidth();
		output.height = image.getHeight();
		output.channels = image.getNumChannels();
		output.data = image.getData();
		output.gpudata = &texture;
	}


	//asset path
	void FileManager::setAssetPath(const std::string& path) {
		asset_path = path;
	}
	const std::string& FileManager::getAssetPath() const {
		return asset_path;
	}

	//file selection
	void FileManager::setFile(std::string filename) {
		std::cout << "------------------------------------------------------------" << std::endl;
		output.filename = asset_path + filename;
		internalLoad(output.filename);
	}
	void FileManager::cycleFileNext(std::string from) {
		std::cout << "------------------------------------------------------------" << std::endl;
		if (from != "") output.filename = asset_path + from;
		std::string file = internalNextFile(output.filename, true);
		if (file != "") {
			output.filename = asset_path +internalNextFile(output.filename, true);
			internalLoad(output.filename);
		}
		else std::cout<< " --- [FileManager] ERROR the input folder is empty " << std::endl;
	}
	void FileManager::cycleFilePrev(std::string from) {
		std::cout << "------------------------------------------------------------" << std::endl;
		if (from != "") output.filename = asset_path + from;
		std::string file = internalNextFile(output.filename, true);
		if (file != "") {
			output.filename = asset_path + internalNextFile(output.filename, false);
			internalLoad(output.filename);
		}
		else std::cout << " --- [FileManager] ERROR the input folder is empty " << std::endl;
	}

	//output and control
	const std::string& FileManager::getFilename() const{
		return output.filename;
	}
	unsigned int FileManager::getWidth() const{
		return output.width;
	}
	unsigned int FileManager::getHeight() const{
		return output.height;
	}
	unsigned int FileManager::getNumChannels() const{
		return output.channels;
	}

	const unsigned char* FileManager::getData() const{
		return output.data;
	}
	const GPUTexture2D* FileManager::getDataGPU() const{
		return output.gpudata;
	}

	const FileOutput& FileManager::getFileOutput() const{
		return output;
	}


}
