///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "renderer.h"
#include "dependencies/lap_image/lap_image.hpp"

#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))

namespace edge{

	Renderer::Renderer(){
		//full screen
		fs_shader.load("../shaders/renderer_fullscreen.vert", "../shaders/renderer_fullscreen.frag");

		//fullscreen geometry and vao
		struct FullscreenVertex {
			FullscreenVertex(float ix, float iy, float iuvx, float iuvy) : x(ix), y(iy), uvx(iuvx), uvy(iuvy) {}
			float x, y, uvx, uvy;
		};
		std::vector<FullscreenVertex> fullscreendata;
		fullscreendata.emplace_back(-1.f, 1.f, 0.f, 0.f);
		fullscreendata.emplace_back(1.f, 1.f, 1.f, 0.f);
		fullscreendata.emplace_back(1.f, -1.f, 1.f, 1.f);
		fullscreendata.emplace_back(1.f, -1.f, 1.f, 1.f);
		fullscreendata.emplace_back(-1.f, -1.f, 0.f, 1.f);
		fullscreendata.emplace_back(-1.f, 1.f, 0.f, 0.f);
		glGenVertexArrays(1, &fs_vao);
		glBindVertexArray(fs_vao);
		glGenBuffers(1, &fs_vbo); glBindBuffer(GL_ARRAY_BUFFER, fs_vbo);
		glBufferData(GL_ARRAY_BUFFER, fullscreendata.size() * sizeof(FullscreenVertex), fullscreendata.data(), GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(FullscreenVertex), (GLvoid*)0);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(FullscreenVertex), (GLvoid*)(sizeof(float)*2));

		//fullscreen sampling
		fs_sampler.setNearest();
		fs_sampler.setClamp();

		//noinput
		fs_texture_noinput.load("../input/noinput.png", 3, false, -1, false);
	}
	Renderer::~Renderer(){
		//fullscreen
		glDeleteVertexArrays(1, &fs_vao);
		glDeleteBuffers(1, &fs_vbo);
	}


	void Renderer::setInput(const GPUTexture2D* in_input) {
		input = in_input;
	}
	void Renderer::setInputFilteringNearest() {
		fs_sampler.setNearest();
	}
	void Renderer::setInputFilteringBilinear() {
		fs_sampler.setBilinear();
	}
	

	void Renderer::setOutputPath(const std::string& path) {
		output_path = path;
	}
	void Renderer::screenshot(const std::string& filename) {
		assert(fs_width > 0 && fs_height > 0);
		std::cout << "--- [RENDERER] Screenshot saved to file [" << output_path + filename << "] " << std::endl;
		//readback from default framebuffer
		std::vector<unsigned char> pixels; pixels.resize(fs_width * fs_height * 4);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glReadnPixels(0, 0, fs_width, fs_height, GL_RGBA, GL_UNSIGNED_BYTE, (unsigned int)pixels.size(), &pixels[0]);

		//flip,save
		lap::Image img{ pixels.data(), fs_width, fs_height, 4 };
		img.transformFlipY();
		img.save(output_path + filename);
	}

	void Renderer::render() {
		glViewport(0, 0, (int)fs_width, (int)fs_height);
		glClearColor(0.5, 0.5, 0.5, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		fs_sampler.bind(0);
		fs_shader.bind();
		if (!input) fs_texture_noinput.bindAsTexture(0);
		else input->bindAsTexture(0);

		glBindVertexArray(fs_vao);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}
	
	void Renderer::resize(unsigned int width, unsigned int height){
		fs_width = width;
		fs_height = height;
	}
}
