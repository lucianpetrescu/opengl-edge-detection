///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "gpu_buffer.h"
#include <cstdint>
#include <sstream>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <cstring>
#include <cassert>

namespace edge{

	//ctor
	GPUBuffer::GPUBuffer(GLenum in_storage_flags) {
		storage_flags = in_storage_flags;
		resize(1);
	}
	GPUBuffer::GPUBuffer(size_t in_size, GLenum in_storage_flags) {
		storage_flags = in_storage_flags;
		resize(in_size);
	}
	GPUBuffer::GPUBuffer(void* data, size_t in_offset, size_t in_size, GLenum in_storage_flags) {
		storage_flags = in_storage_flags;
		resize(in_size);
		update(data, in_offset, in_size);
	}
	GPUBuffer::~GPUBuffer() {
		if (globject != 0) glDeleteBuffers(1, &globject);
	}
	GPUBuffer::GPUBuffer(GPUBuffer&& rhs) {
		assert(this != &rhs);	//most definately a user bug as this leads to undefined behavior
		globject = rhs.globject;
		size = rhs.size;
		storage_flags = rhs.storage_flags;
		mapped = rhs.mapped;
		rhs.globject = 0;
		rhs.size = 0;
		rhs.storage_flags = 0;
		rhs.mapped = false;
	}
	GPUBuffer& GPUBuffer::operator=(GPUBuffer&& rhs) {
		assert(this != &rhs);	//most definately a user bug as this leads to undefined behavior
		if (globject != 0){
			glDeleteBuffers(1, &globject);
			globject = 0;
		}
		globject = rhs.globject;
		size = rhs.size;
		storage_flags = rhs.storage_flags;
		mapped = rhs.mapped;
		rhs.globject = 0;
		rhs.size = 0;
		rhs.storage_flags = 0;
		rhs.mapped = false;
		return (*this);
	}

	GPUBuffer GPUBuffer::clone() const {
		assert(globject != 0);
		GPUBuffer result{ storage_flags };
		result.resize(size);
		glCopyNamedBufferSubData(globject, result.getGLobject(), 0, 0, size);
		return result; //nrvo move
	}

	//to and from gpu
	void GPUBuffer::resize(size_t in_size) {
		assert(in_size >0);

		//the memory contract still stands, no resize needed
		if (in_size == size) return;

		if (globject != 0) glDeleteBuffers(1, &globject);
		glCreateBuffers(1, &globject);

		size = in_size;
		glNamedBufferStorage(globject, size, nullptr, storage_flags);
	}
	void GPUBuffer::copy(void* dst, size_t in_gpubufferoffset, size_t in_datasize) const {
		assert(globject != 0);
		GLvoid* ptr = glMapNamedBufferRange(globject, 0, size, GL_MAP_READ_BIT);
		std::memcpy(dst, reinterpret_cast<unsigned char*>(ptr) + in_gpubufferoffset, in_datasize);
		glUnmapNamedBuffer(globject);
	}
	void GPUBuffer::update(void* data, size_t in_gpubufferoffset, size_t in_datasize) {
		assert(globject != 0);
		glNamedBufferSubData(globject, in_gpubufferoffset, in_datasize, reinterpret_cast<unsigned char*>(data));
	}
	void GPUBuffer::update(void* data) {
		assert(globject != 0);
		glNamedBufferSubData(globject, 0, size, reinterpret_cast<unsigned char*>(data));
	}

	
	//binds
	void GPUBuffer::bind(GLenum target) {
		assert(globject != 0);
		glBindBuffer(target, globject);
	}
	void GPUBuffer::bindIndexed(GLenum target, unsigned int index) {
		assert(globject != 0);
		bindIndexed(target, index, 0, size);
	}
	void GPUBuffer::bindIndexed(GLenum target, unsigned int index, size_t offset, size_t in_size){
		assert(globject != 0);
		if (target == GL_ATOMIC_COUNTER_BUFFER || target == GL_TRANSFORM_FEEDBACK_BUFFER || target == GL_UNIFORM_BUFFER || target == GL_SHADER_STORAGE_BUFFER) {
			glBindBufferRange(target, index, globject, (GLintptr)offset, (GLsizeiptr)in_size);
		}
	}

	//maps
	void* GPUBuffer::map(size_t offset, GLenum access) {
		assert(globject != 0 && !mapped);
		GLvoid* ptr = glMapNamedBuffer(globject, access);
		mapped = true;
		return (void*)((unsigned char*)ptr + offset);
	}
	void GPUBuffer::unmap() {
		assert(globject != 0 && mapped);
		glUnmapNamedBuffer(globject);
		mapped = false;
	}

	//raw acess
	GLuint GPUBuffer::getGLobject() const {
		return globject;
	}
	size_t GPUBuffer::getSize() const {
		return size;
	}
	GLenum GPUBuffer::getStorageFlags() const {
		return storage_flags;
	}
	bool GPUBuffer::getIsMapped() const {
		return mapped;
	}
}
