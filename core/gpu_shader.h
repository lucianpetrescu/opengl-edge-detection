///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------

#pragma once
//we need OpenGL extensions, and lap_wic provides them
#include "../dependencies/lap_wic/lap_wic.hpp"
//we need vec3, mat4, etc, glm provides it
#include "../dependencies/glm/glm.hpp"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>

namespace edge {

	// In OpenGL parlance this class defines a GPU Program, composed of many shader objects, but we'll use the GPUShader name for
	// the entire GPU Program due to its common usage.
	class GPUShader {
	public:
		//empty
		GPUShader();
		//creates a gpu program from a vertex shader file and a fragment shader file
		GPUShader(const std::string &vertex_shader_file, const std::string &fragment_shader_file);
		//creates a gpu program from a vertex shader file, a geometry shader file and a fragment shader file
		GPUShader(const std::string &vertex_shader_file, const std::string &geometry_shader_file, const std::string &fragment_shader_file);
		//creates a gpu program from a vertex shader file, a tess control file, a tess eval file and a fragment shader
		GPUShader(const std::string &vertex_shader_file, const std::string &tessellation_control_shader_file, const std::string &tessellation_evaluation_shader_file, const std::string &fragment_shader_file);
		//creates a gpu program from a vertex shader file, a tess control file, a tess eval file, a geometry shader and a fragment shader
		GPUShader(const std::string &vertex_shader_file, const std::string &tessellation_control_shader_file, const std::string &tessellation_evaluation_shader_file, const std::string& geometry_shader_file, const std::string &fragment_shader_file);
		//creates a gpu program from a compute shader
		GPUShader(const std::string &compute_shader_file);

		//explicitly forbid implicit copies, use clone for explicit copy
		GPUShader(const GPUShader&) = delete;
		GPUShader& operator=(const GPUShader&) = delete;
		//allow moves
		GPUShader(GPUShader&&);
		GPUShader& operator=(GPUShader&&);

		//clones this object(creates a new OpenGL object), expensive!
		GPUShader clone() const;
		
		//creates a gpu program from a vertex shader file and a fragment shader file
		void load(const std::string &vertex_shader_file, const std::string &fragment_shader_file);
		//creates a gpu program from a vertex shader file, a geometry shader file and a fragment shader file
		void load(const std::string &vertex_shader_file, const std::string &geometry_shader_file, const std::string &fragment_shader_file);
		//creates a gpu program from a vertex shader file, a tess control file, a tess eval file and a fragment shader
		void load(const std::string &vertex_shader_file, const std::string &tessellation_control_shader_file, const std::string &tessellation_evaluation_shader_file, const std::string &fragment_shader_file);
		//creates a gpu program from a vertex shader file, a tess control file, a tess eval file, a geometry shader and a fragment shader
		void load(const std::string &vertex_shader_file, const std::string &tessellation_control_shader_file, const std::string &tessellation_evaluation_shader_file, const std::string& geometry_shader_file, const std::string &fragment_shader_file);
		//creates a gpu program from a compute shader
		void load(const std::string &compute_shader_file);


		//destructor
		~GPUShader();

		//returns the raw OpenGL program object
		GLuint getOpenGLObject();
		
		//binds the program object (=glUseProgram(getOpenGLObject())
		void bind();

		//reloads the shader from the source files
		void reload();

		//sets uniform
		void setUniform(const std::string& name, float value);
		void setUniform(const std::string& name, int value);
		void setUniform(const std::string& name, unsigned int value);
		void setUniform(const std::string& name, glm::vec2 value);
		void setUniform(const std::string& name, glm::ivec2 value);
		void setUniform(const std::string& name, glm::uvec2 value);
		void setUniform(const std::string& name, glm::vec3 value);
		void setUniform(const std::string& name, glm::ivec3 value);
		void setUniform(const std::string& name, glm::uvec3 value);
		void setUniform(const std::string& name, glm::vec4 value);
		void setUniform(const std::string& name, glm::ivec4 value);
		void setUniform(const std::string& name, glm::uvec4 value);
		void setUniform(const std::string& name, glm::mat2 value, bool transpose = false);
		void setUniform(const std::string& name, glm::mat3 value, bool transpose = false);
		void setUniform(const std::string& name, glm::mat3x4 value, bool transpose = false);
		void setUniform(const std::string& name, glm::mat4 value, bool transpose = false);

		//subroutines - only call this function on active programs
		void setSubroutine(const std::string& name, GLenum shader_type);
	private:
		int getUniformLocation(const std::string& name);
		void createProgram();


		std::map<std::string, int> uniform_locations;
		std::vector<std::pair<std::string, GLenum>> contents;
		GLuint glprogram = 0;
	};
	


	

}
