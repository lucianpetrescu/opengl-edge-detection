///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "gpu_timer.h"
#include <cassert>

namespace edge{

	//----------------------------------------------------------------------
	//ctor dtor
	GPUTimer::GPUTimer(unsigned int count){
		resize(count);
	}
	GPUTimer::~GPUTimer(){
		if (queries.size() > 0) glDeleteQueries((GLsizei)queries.size(), &queries[0]);
		queries.clear();
		cpu_times.clear();
		gpu_times.clear();
	}


	//state
	void GPUTimer::resize(unsigned int count) {
		queries.resize(count);
		gpu_times.resize(count);
		cpu_times.resize(count);
		if (queries.size() > 0) glDeleteQueries((GLsizei)queries.size(), &queries[0]);
		if (count > 0) glGenQueries(count, &queries[0]);
		origin = std::chrono::high_resolution_clock::now();
	}
	void GPUTimer::reset(){
		origin = std::chrono::high_resolution_clock::now();
	}
	void GPUTimer::insertEntry(unsigned int index){
		assert(index < queries.size());
		glQueryCounter(queries[index], GL_TIMESTAMP);
		cpu_times[index] = (long long)(std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - origin).count() * 1000);
	}
	unsigned int GPUTimer::getEntryCount(){
		return (unsigned int)queries.size();
	}
	void GPUTimer::synchronizeWithGPU(){
		for (unsigned int i = 0; i < queries.size(); i++){
			GLint done = GL_FALSE;
			while (done != GL_FALSE) glGetQueryObjectiv(queries[i], GL_QUERY_RESULT_AVAILABLE, &done);

			GLuint64 value;
			glGetQueryObjectui64v(queries[i], GL_QUERY_RESULT, &value);
			gpu_times[i] = (long long)value/1000000;  //nano to milli
		}
	}


	//queries
	long long GPUTimer::getEntryTimeInMillisecondsGPU(unsigned int index){
		assert(index < queries.size());
		return gpu_times[index];
	}
	long long GPUTimer::getEntryTimeInMillisecondsCPU(unsigned int index){
		assert(index < queries.size());
		return cpu_times[index];
	}
	long long GPUTimer::getTimeBetweenEntriesInMillisecondsGPU(unsigned int index1, unsigned int index2) {
		if (index1>index2) return getEntryTimeInMillisecondsGPU(index1) - getEntryTimeInMillisecondsGPU(index2);
		else return getEntryTimeInMillisecondsGPU(index2) - getEntryTimeInMillisecondsGPU(index1);
	}
	long long GPUTimer::getTimeBetweenEntriesInMillisecondsCPU(unsigned int index1, unsigned int index2) {
		if (index1>index2) return getEntryTimeInMillisecondsCPU(index1) - getEntryTimeInMillisecondsCPU(index2);
		else return getEntryTimeInMillisecondsCPU(index2) - getEntryTimeInMillisecondsCPU(index1);
	}
	long long GPUTimer::getElapsedMillisecondsCPU(){
		return (long long)(std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - origin).count() * 1000);
	}
}