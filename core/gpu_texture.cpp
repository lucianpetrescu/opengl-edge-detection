﻿///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------

//need an image loading and (mini) processign framework
#include "../dependencies/lap_image/lap_image.hpp"

#include "gpu_texture.h"
#include <fstream>
#include <iostream>
#include <cmath>
#include <cassert>

namespace edge {

	///--------------------------------------------------------------------------------------------------------------------------
	GPUTextureSampler::GPUTextureSampler() {
		glCreateSamplers(1, &globject);
		setNearest();
		setClamp();
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max_anisotropy);
	}
	GPUTextureSampler::~GPUTextureSampler() {
		if(globject !=0) glDeleteSamplers(1, &globject);
	}
	GPUTextureSampler::GPUTextureSampler(GPUTextureSampler&& rhs) {
		assert(this != &rhs);
		max_anisotropy = rhs.max_anisotropy;
		globject = rhs.globject;
		rhs.max_anisotropy = 0;
		rhs.globject = 0;
	}
	GPUTextureSampler& GPUTextureSampler::operator=(GPUTextureSampler&& rhs) {
		assert(this != &rhs);
		if (globject != 0) {
			glDeleteSamplers(1, &globject);
			globject = 0;
		}
		max_anisotropy = rhs.max_anisotropy;
		globject = rhs.globject;
		rhs.max_anisotropy = 0;
		rhs.globject = 0;
		return (*this);
	}
	GPUTextureSampler GPUTextureSampler::clone() const {
		GPUTextureSampler result;
		result.max_anisotropy = max_anisotropy;
		GLint param;
		glGetSamplerParameteriv(globject, GL_TEXTURE_WRAP_S, &param);
		glSamplerParameteri(result.globject, GL_TEXTURE_WRAP_S, param);
		glGetSamplerParameteriv(globject, GL_TEXTURE_WRAP_T, &param);
		glSamplerParameteri(result.globject, GL_TEXTURE_WRAP_T, param);
		glGetSamplerParameteriv(globject, GL_TEXTURE_WRAP_R, &param);
		glSamplerParameteri(result.globject, GL_TEXTURE_WRAP_R, param);
		glGetSamplerParameteriv(globject, GL_TEXTURE_MIN_FILTER, &param);
		glSamplerParameteri(result.globject, GL_TEXTURE_MIN_FILTER, param);
		glGetSamplerParameteriv(globject, GL_TEXTURE_MAG_FILTER, &param);
		glSamplerParameteri(result.globject, GL_TEXTURE_MAG_FILTER, param);
		glGetSamplerParameteriv(globject, GL_TEXTURE_MAX_ANISOTROPY_EXT, &param);
		glSamplerParameteri(result.globject, GL_TEXTURE_MAX_ANISOTROPY_EXT, param);
		return result;//nrvo
	}

	void GPUTextureSampler::bind(unsigned int texture_unit) const {
		glBindSampler(texture_unit, globject);
	}
	void GPUTextureSampler::setRepeat() {
		glSamplerParameterf(globject, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameterf(globject, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glSamplerParameterf(globject, GL_TEXTURE_WRAP_R, GL_REPEAT);
	}
	void GPUTextureSampler::setClamp() {
		glSamplerParameterf(globject, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glSamplerParameterf(globject, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glSamplerParameterf(globject, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	}
	void GPUTextureSampler::setNearest() {
		glSamplerParameterf(globject, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameterf(globject, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
	void GPUTextureSampler::setBilinear() {
		glSamplerParameterf(globject, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameterf(globject, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	void GPUTextureSampler::setTrilinear() {
		glSamplerParameterf(globject, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameterf(globject, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameterf(globject, GL_TEXTURE_MAX_ANISOTROPY_EXT, 1);
	}
	void GPUTextureSampler::setTrilinearAnisotropic(float anisotropy) {
		glSamplerParameterf(globject, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameterf(globject, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		//clamp to device max
		if (anisotropy > max_anisotropy) glSamplerParameterf(globject, GL_TEXTURE_MAX_ANISOTROPY_EXT, max_anisotropy);
		else glSamplerParameterf(globject, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropy);
	}
	float GPUTextureSampler::getMaxDeviceAnisotropy() {
		return max_anisotropy;
	}
	unsigned int GPUTextureSampler::getGLobject() const {
		return globject;
	}


	///--------------------------------------------------------------------------------------------------------------------------
	//forward definition
	size_t helperComputeMemoryUsageInBytes(GLuint globject);

	//ctors and operators
	GPUTexture2D::GPUTexture2D() {
	}
	GPUTexture2D::GPUTexture2D(const std::string& filename, unsigned int max_channels, bool in_mipmaps, int in_mipmaps_max_level, bool flipy){
		load(filename, max_channels, in_mipmaps, in_mipmaps_max_level, flipy);
	}
	GPUTexture2D::GPUTexture2D(GLint in_internal_format, unsigned int data_width, unsigned int data_height, bool in_mipmaps, int in_mipmaps_max_level){
		create(in_internal_format, data_width, data_height, in_mipmaps, in_mipmaps_max_level);
	}
	GPUTexture2D::GPUTexture2D(GLint in_internal_format, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data, bool in_mipmaps, int in_mipmaps_max_level){
		create(in_internal_format, data_width, data_height, in_mipmaps, in_mipmaps_max_level);
		update(data_format, data_type, data, in_mipmaps);
	}
	GPUTexture2D::~GPUTexture2D() {
		if(globject !=0) glDeleteTextures(1, &globject);
	}
	GPUTexture2D::GPUTexture2D(GPUTexture2D&& rhs){
		assert(this != &rhs);	//user bug 
		width = rhs.width;
		height = rhs.height;
		memory_base = rhs.memory_base;
		memory_with_mipmaps = rhs.memory_with_mipmaps;
		internal_format = rhs.internal_format;
		globject = rhs.globject;
		mipmaps = rhs.mipmaps;
		mipmaps_max_level = rhs.mipmaps_max_level;
		rhs.width = 0;
		rhs.height = 0;
		rhs.memory_base = 0;
		rhs.memory_with_mipmaps = 0;
		rhs.internal_format = 0;
		rhs.globject = 0;
		rhs.mipmaps = false;
		rhs.mipmaps_max_level = 0;
	}
	GPUTexture2D& GPUTexture2D::operator=(GPUTexture2D&& rhs) {
		assert(this != &rhs);	//user bug 
		if (globject != 0) {
			glDeleteTextures(1, &globject);
			globject = 0;
		}
		width = rhs.width;
		height = rhs.height;
		memory_base = rhs.memory_base;
		memory_with_mipmaps = rhs.memory_with_mipmaps;
		internal_format = rhs.internal_format;
		globject = rhs.globject;
		mipmaps = rhs.mipmaps;
		mipmaps_max_level = rhs.mipmaps_max_level;
		rhs.width = 0;
		rhs.height = 0;
		rhs.memory_base = 0;
		rhs.memory_with_mipmaps = 0;
		rhs.internal_format = 0;
		rhs.globject = 0;
		rhs.mipmaps = false;
		rhs.mipmaps_max_level = 0;
		return (*this);
	}
	GPUTexture2D GPUTexture2D::clone() {
		GPUTexture2D result;
		result.create(internal_format, width, height, mipmaps, mipmaps_max_level);
		for (unsigned int i = 0; i < mipmaps_max_level; i++) {
			GLint w, h;
			glGetTextureLevelParameteriv(globject, i, GL_TEXTURE_WIDTH, &w);
			glGetTextureLevelParameteriv(globject, i, GL_TEXTURE_HEIGHT, &h);
			glCopyImageSubData(globject, GL_TEXTURE_2D, i, 0, 0, 0, result.globject, GL_TEXTURE_2D, i, 0, 0, 0, w, h, 1);
		}
		return result;//nrvo
	}
	void GPUTexture2D::swap(GPUTexture2D& other) {
		std::swap(width, other.width);
		std::swap(height, other.height);
		std::swap(memory_base, other.memory_base);
		std::swap(memory_with_mipmaps, other.memory_with_mipmaps);
		std::swap(internal_format, other.internal_format);
		std::swap(globject, other.globject);
		std::swap(mipmaps, other.mipmaps);
		std::swap(mipmaps_max_level, other.mipmaps_max_level);
	}

	//re-create or resize
	void GPUTexture2D::create(GLint in_internal_format, unsigned int data_width, unsigned int data_height, bool in_mipmaps, int in_mipmaps_max_level) {
		create(in_internal_format, data_width, data_height, GL_RGB, GL_UNSIGNED_BYTE, nullptr, in_mipmaps, in_mipmaps_max_level);
	}
	void GPUTexture2D::load(const std::string& filename, unsigned int max_channels, bool in_mipmaps, int in_mipmaps_max_level, bool flipy) {
		lap::Image image(filename);
		unsigned int data_width = image.getWidth();
		unsigned int data_height = image.getHeight();
		
		//limit channels to what the user requests (e.g. want 3 channels from a 4 channel texture, or just one channel from a 3 channel texture)
		unsigned int channels = std::min(max_channels, image.getNumChannels());
		lap::Image img{ 1,1,channels };
		unsigned char* data = image.getData();
		if(flipy) image.transformFlipY();	
		if (channels < image.getNumChannels()) {
			img.resize(data_width, data_height);
			for (unsigned int i = 0; i < data_width; i++) {
				for (unsigned int j = 0; j < data_height; j++) {
					for (unsigned int c = 0; c < channels; c++) {
						img(i, j, c) = image(i, j, c);
					}
				}
			}
			data = img.getData();
		}

		GLenum internal_format = GL_RGB8, data_format = GL_RGB;
		if (channels == 1) {
			internal_format = GL_R8;
			data_format = GL_RED;
		}
		else if (channels == 2) {
			internal_format = GL_RG8;
			data_format = GL_RG;
		}
		else if (channels == 3) {
			internal_format = GL_RGB8;
			data_format = GL_RGB;
		}
		else if (channels == 4) {
			internal_format = GL_RGBA8;
			data_format = GL_RGBA;
		}

		create(internal_format, data_width, data_height, in_mipmaps, in_mipmaps_max_level);
		update(data_format, GL_UNSIGNED_BYTE, data, in_mipmaps);
	}
	void GPUTexture2D::create(GLint in_internal_format, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data, bool in_mipmaps, int in_mipmaps_max_level) {
		//compute the structure of the created texture
		unsigned int new_mipmaps_max_level = 1;
		if (!in_mipmaps) new_mipmaps_max_level = 1;																				//no mipmaps
		else {
			new_mipmaps_max_level = (unsigned int)(std::log2(std::max(data_width, data_height)) + 1);											//compute mipmaps, full chain
			if (in_mipmaps_max_level !=-1 && in_mipmaps_max_level<(int)new_mipmaps_max_level) new_mipmaps_max_level = in_mipmaps_max_level;		//limit to requested (if reasonable)
		}

		// no storage space changes? -> quick exit
		if (in_internal_format == internal_format && data_width == width && data_height == height && mipmaps == in_mipmaps && mipmaps_max_level == new_mipmaps_max_level)  return;
		
		//the storage contract does not exist or it can't be used anymore, another storage space has to be allocated from scratch.
		if (globject != 0) glDeleteTextures(1, &globject);
		glCreateTextures(GL_TEXTURE_2D, 1, &globject);
		internal_format = in_internal_format;
		width = data_width;
		height = data_height;
		mipmaps = in_mipmaps;
		mipmaps_max_level = new_mipmaps_max_level;

		//allocate storage for base + ALL mipmap levels in one operation.
		glTextureStorage2D(globject, mipmaps_max_level, internal_format, width, height);

		//compute memory usage 
		memory_base = helperComputeMemoryUsageInBytes(globject);
		//add mipmaps to memory usage
		memory_with_mipmaps = memory_base;
		if (mipmaps) {
			float bytespp = (float)memory_base / width / height;
			int w = width;
			int h = height;
			for (unsigned int i = 1; i < mipmaps_max_level; i++) {
				w = std::max(1, (w / 2));
				h = std::max(1, (h / 2));
				memory_with_mipmaps += (size_t)(bytespp * w * h);
			}
		}

		//put data in the allocated storage
		if (data) update(data_format, data_type, data, in_mipmaps);

	}
	void GPUTexture2D::resize(unsigned int in_width, unsigned int in_height, bool in_mipmaps, int in_mipmaps_max_level) {
		//recreate with new width and height
		create(internal_format, in_width, in_height, in_mipmaps, in_mipmaps_max_level);
	}

	//data transfers
	void GPUTexture2D::update(GLenum data_format, GLenum data_type, const GLvoid* data, bool update_mipmaps) {
		assert(globject != 0);
		//guarantee alignment
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		//update texture
		glTextureSubImage2D(globject, 0, 0, 0, width, height, data_format, data_type, data);
		if (update_mipmaps && mipmaps) glGenerateTextureMipmap(globject);
	}
	void GPUTexture2D::update(unsigned int level, unsigned int offset_x, unsigned int offset_y, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data){
		assert(globject != 0);
		//guarantee alignment
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		//update texture
		glTextureSubImage2D(globject, level, offset_x, offset_y, data_width, data_height, data_format, data_type, data);
	}
	void GPUTexture2D::updateMipmaps() {
		assert(globject != 0);
		if(mipmaps) glGenerateTextureMipmap(globject);
	}
	void GPUTexture2D::readToCPU(unsigned int level, GLenum outdata_format, GLenum outdata_type, GLsizei outdata_buffer_size, void* outdata) const {
		assert(globject != 0);
		//guarantee alignment
		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		//read
		glGetTextureImage(globject, level, outdata_format, outdata_type, outdata_buffer_size, outdata);
	}

	//usage
	void GPUTexture2D::bindAsTexture(unsigned int texture_unit) const {
		glBindTextureUnit(texture_unit, globject);
	}
	void GPUTexture2D::bindAsImage(unsigned int image_unit, GLenum access, unsigned int level, GLenum format) const{
		glBindImageTexture(image_unit, globject, level, GL_FALSE, 0, access, format);
	}

	//access
	GLint GPUTexture2D::getInternalFormat() const {
		return internal_format;
	}
	bool GPUTexture2D::getMipmapsExist() const {
		return mipmaps;
	}
	unsigned int GPUTexture2D::getMipmapsMaxLevel() const {
		return mipmaps_max_level;
	}

	unsigned int GPUTexture2D::getWidth() const {
		return width;
	}
	unsigned int GPUTexture2D::getHeight() const {
		return height;
	}
	unsigned int GPUTexture2D::getGLobject() const {
		return globject;
	}

	//memory
	size_t GPUTexture2D::getMemoryBase() const {
		return memory_base;
	}
	size_t GPUTexture2D::getMemoryWithMipmaps() const {
		return memory_with_mipmaps;
	}


	//internal help (
	size_t helperComputeMemoryUsageInBytes(GLuint globject){
		//if the texture is in a compressed format then we can query it directly
		GLint iscompressed = 0;
		glGetTextureLevelParameteriv(globject, 0, GL_TEXTURE_COMPRESSED, &iscompressed);
		if (iscompressed == GL_TRUE) {
			GLint compressed_size = 0;
			glGetTextureLevelParameteriv(globject, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &compressed_size);
			if(compressed_size !=0) return compressed_size;
		}
		
		//else, we need to query the globject properties. This has to be done manually, as there is no help api available, and yes, 
		// in terms of monitoring support OpenGL is clearly inferior to other APIs.
		GLint format, w, h;
		glGetTextureLevelParameteriv(globject, 0, GL_TEXTURE_WIDTH, &w);
		glGetTextureLevelParameteriv(globject, 0, GL_TEXTURE_HEIGHT, &h);
		glGetTextureLevelParameteriv(globject, 0, GL_TEXTURE_INTERNAL_FORMAT, &format);

		switch (format) {
		//some of these are guesses on size, all follow the same  bytes_per_pixel * w * h logic
		//uncompressed
		case GL_DEPTH_COMPONENT:	return (size_t)(4 * w * h);
		case GL_DEPTH_STENCIL:		return (size_t)(4 * w * h);
		case GL_RED:				return (size_t)(1 * w * h);
		case GL_RG:					return (size_t)(2 * w * h);
		case GL_RGB:				return (size_t)(3 * w * h);
		case GL_RGBA:				return (size_t)(4 * w * h);
		case GL_DEPTH_COMPONENT16:	return (size_t)(2 * w * h);
		case GL_DEPTH_COMPONENT24:  return (size_t)(3 * w * h);
		case GL_DEPTH_COMPONENT32:  return (size_t)(4 * w * h);
		case GL_DEPTH_COMPONENT32F: return (size_t)(4 * w * h);
		case GL_DEPTH24_STENCIL8:	return (size_t)(4 * w * h);
		case GL_DEPTH32F_STENCIL8:	return (size_t)(5 * w * h);
		case GL_STENCIL_INDEX1:		return (size_t)(0.125 * w * h);
		case GL_STENCIL_INDEX4:		return (size_t)(0.5 * w * h);
		case GL_STENCIL_INDEX8:		return (size_t)(1 * w * h);
		case GL_STENCIL_INDEX16:	return (size_t)(2 * w * h);
		case GL_R8:					return (size_t)(1 * w * h);
		case GL_R8_SNORM:			return (size_t)(1 * w * h);
		case GL_R16:				return (size_t)(2 * w * h);
		case GL_R16_SNORM:			return (size_t)(2 * w * h);
		case GL_RG8:				return (size_t)(2 * w * h);
		case GL_RG8_SNORM:			return (size_t)(2 * w * h);
		case GL_RG16:				return (size_t)(4 * w * h);
		case GL_RG16_SNORM:			return (size_t)(4 * w * h);
		case GL_R3_G3_B2:			return (size_t)(1 * w * h);
		case GL_RGB4:				return (size_t)(1.5 * w * h);
		case GL_RGB5:				return (size_t)(1.875 * w * h);
		case GL_RGB8:				return (size_t)(3 * w * h);
		case GL_RGB8_SNORM:			return (size_t)(3 * w * h);
		case GL_RGB10:				return (size_t)(3.75 * w * h);
		case GL_RGB12:				return (size_t)(4.5 * w * h);
		case GL_RGB16_SNORM:		return (size_t)(6 * w * h);
		case GL_RGBA2:				return (size_t)(1 * w * h);
		case GL_RGBA4:				return (size_t)(2 * w * h);
		case GL_RGB5_A1:			return (size_t)(2 * w * h);
		case GL_RGBA8:				return (size_t)(4 * w * h);
		case GL_RGBA8_SNORM:		return (size_t)(4 * w * h);
		case GL_RGB10_A2:			return (size_t)(4 * w * h);
		case GL_RGB10_A2UI:			return (size_t)(4 * w * h);
		case GL_RGBA12:				return (size_t)(6 * w * h);
		case GL_RGBA16:				return (size_t)(8 * w * h);
		case GL_SRGB8:				return (size_t)(3 * w * h);
		case GL_SRGB8_ALPHA8:		return (size_t)(4 * w * h);
		case GL_R16F:				return (size_t)(2 * w * h);
		case GL_RG16F:				return (size_t)(2 * w * h);
		case GL_RGB16F:				return (size_t)(6 * w * h);
		case GL_RGBA16F:			return (size_t)(8 * w * h);
		case GL_R32F:				return (size_t)(4 * w * h);
		case GL_RG32F:				return (size_t)(8 * w * h);
		case GL_RGB32F:				return (size_t)(12 * w * h);
		case GL_RGBA32F:			return (size_t)(16 * w * h);
		case GL_R11F_G11F_B10F:		return (size_t)(4 * w * h);
		case GL_RGB9_E5:			return (size_t)(4 * w * h);
		case GL_R8I:				return (size_t)(1 * w * h);
		case GL_R8UI:				return (size_t)(1 * w * h);
		case GL_R16I:				return (size_t)(2 * w * h);
		case GL_R16UI:				return (size_t)(2 * w * h);
		case GL_R32I:				return (size_t)(4 * w * h);
		case GL_R32UI:				return (size_t)(4 * w * h);
		case GL_RG8I:				return (size_t)(2 * w * h);
		case GL_RG8UI:				return (size_t)(2 * w * h);
		case GL_RG16I:				return (size_t)(4 * w * h);
		case GL_RG16UI:				return (size_t)(4 * w * h);
		case GL_RG32I:				return (size_t)(16 * w * h);
		case GL_RG32UI:				return (size_t)(16 * w * h);
		case GL_RGB8I:				return (size_t)(3 * w * h);
		case GL_RGB8UI:				return (size_t)(3 * w * h);
		case GL_RGB16I:				return (size_t)(6 * w * h);
		case GL_RGB16UI:			return (size_t)(6 * w * h);
		case GL_RGB32I:				return (size_t)(12 * w * h);
		case GL_RGB32UI:			return (size_t)(12 * w * h);
		case GL_RGBA8I:				return (size_t)(4 * w * h);
		case GL_RGBA8UI:			return (size_t)(4 * w * h);
		case GL_RGBA16I:			return (size_t)(8 * w * h);
		case GL_RGBA16UI:			return (size_t)(8 * w * h);
		case GL_RGBA32I:			return (size_t)(16 * w * h);
		case GL_RGBA32UI:			return (size_t)(16 * w * h);

		//theoretically these cases should be handled by the if(compressed) return compressedsize at the beginning of this function, some drivers might not offer support..
		case GL_COMPRESSED_RED:		return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RG:		return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGB:		return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RGBA:	return (size_t)(1 * w * h);
		case 0x83F0:				return (size_t)(0.5 * w * h);
		case 0x83F1:				return (size_t)(0.5 * w * h);
		case 0x83F2:				return (size_t)(1 * w * h);
		case 0x83F3:				return (size_t)(1 * w * h);
		case 0x8C4C:				return (size_t)(0.5 * w * h);
		case 0x8C4D:				return (size_t)(0.5 * w * h);
		case 0x8C4E:				return (size_t)(1 * w * h);
		case 0x8C4F:				return (size_t)(1 * w * h);
		case 0x8C48:				return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RED_RGTC1:				return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_SIGNED_RED_RGTC1:		return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RG_RGTC2:				return (size_t)(1 * w * h);
		case GL_COMPRESSED_SIGNED_RG_RGTC2:			return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT:	return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT:	return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGBA_BPTC_UNORM:			return (size_t)(1 * w * h);
		case GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM:	return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGB8_ETC2:				return (size_t)(1 * w * h);
		case GL_COMPRESSED_SRGB8_ETC2:				return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2:	return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2:	return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RGBA8_ETC2_EAC:			return (size_t)(1 * w * h);
		case GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC:	return (size_t)(1 * w * h);
		case GL_COMPRESSED_R11_EAC:					return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_SIGNED_R11_EAC:			return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RG11_EAC:				return (size_t)(1 * w * h);
		case GL_COMPRESSED_SIGNED_RG11_EAC:			return (size_t)(1 * w * h);
		case 0x93B0: return (size_t)(0.5 * w * h);
		case 0x93B1: return (size_t)(0.8 * w * h);
		case 0x93B2: return (size_t)(0.64 * w * h);
		case 0x93B3: return (size_t)(0.53 * w * h);
		case 0x93B4: return (size_t)(0.445 * w * h);
		case 0x93B5: return (size_t)(0.4 * w * h);
		case 0x93B6: return (size_t)(0.333 * w * h);
		case 0x93B7: return (size_t)(0.25 * w * h);
		case 0x93B8: return (size_t)(0.325 * w * h);
		case 0x93B9: return (size_t)(0.266 * w * h);
		case 0x93BA: return (size_t)(0.2 * w * h);
		case 0x93BB: return (size_t)(0.16 * w * h);
		case 0x93BC: return (size_t)(0.133 * w * h);
		case 0x93BD: return (size_t)(0.111 * w * h);
		case 0x93D0: return (size_t)(0.5 * w * h);
		case 0x93D1: return (size_t)(0.11 * w * h);
		case 0x93D2: return (size_t)(0.64 * w * h);
		case 0x93D3: return (size_t)(0.53 * w * h);
		case 0x93D4: return (size_t)(0.445 * w * h);
		case 0x93D5: return (size_t)(0.4 * w * h);
		case 0x93D6: return (size_t)(0.333 * w * h);
		case 0x93D7: return (size_t)(0.25 * w * h);
		case 0x93D8: return (size_t)(0.32 * w * h);
		case 0x93D9: return (size_t)(0.266 * w * h);
		case 0x93DA: return (size_t)(0.2 * w * h);
		case 0x93DB: return (size_t)(0.16 * w * h);
		case 0x93DC: return (size_t)(0.133 * w * h);
		case 0x93DD: return (size_t)(0.111 * w * h);
		default:
			//unsupported format (compatibility or a non-core non-ubiquous non-astc extension), leave everything to default
			break;
		}
		return 0;
	}
}