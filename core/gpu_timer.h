///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
//need OpenGL for GPU queries and lap wic provides them
#include "../dependencies/lap_wic/lap_wic.hpp"
#include <vector>
#include <chrono>
#include <string>

namespace edge{
	//----------------------------------------------------------------------
	// simple gpu timer
	class GPUTimer{
	public:
		//ctor dtor
		GPUTimer(unsigned int count =0 );
		GPUTimer(const GPUTimer&) = delete;
		GPUTimer(GPUTimer&&) = delete;
		GPUTimer& operator=(const GPUTimer&) = delete;
		GPUTimer& operator=(GPUTimer&&) = delete;
		~GPUTimer();
		
		//state and insert
		void resize(unsigned int count);
		unsigned int getEntryCount();
		void synchronizeWithGPU();
		void reset();

		//queries
		void insertEntry(unsigned int index);
		long long getElapsedMillisecondsCPU();
		long long getEntryTimeInMillisecondsGPU(unsigned int index);
		long long getEntryTimeInMillisecondsCPU(unsigned int index);
		long long getTimeBetweenEntriesInMillisecondsGPU(unsigned int index1, unsigned int index2);
		long long getTimeBetweenEntriesInMillisecondsCPU(unsigned int index1, unsigned int index2);
	private:
		std::vector<unsigned int> queries;
		std::vector<long long> gpu_times;
		std::vector<long long> cpu_times;
		std::chrono::time_point<std::chrono::high_resolution_clock> origin;
	};
}