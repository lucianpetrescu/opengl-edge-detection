///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------

#pragma once
// module needs OpenGL and wic provides it
#include "../dependencies/lap_wic/lap_wic.hpp"

namespace edge{

	///--------------------------------------------------------------------------------------------------------------------------
	class GPUTextureSampler {
	public:
		GPUTextureSampler();
		//disable straight copies, use clone
		GPUTextureSampler(const GPUTextureSampler&) = delete;
		GPUTextureSampler& operator=(const GPUTextureSampler&) = delete;
		//but enable move semantics
		GPUTextureSampler(GPUTextureSampler&&);
		GPUTextureSampler& operator=(GPUTextureSampler&&);
		~GPUTextureSampler();
		GPUTextureSampler clone() const;
		void bind(unsigned int texture_unit) const;
		void setRepeat();
		void setClamp();
		void setNearest();
		void setBilinear();
		void setTrilinear();
		void setTrilinearAnisotropic(float anisotropy);
		float getMaxDeviceAnisotropy();
		unsigned int getGLobject() const;
	private:
		float max_anisotropy =0;
		unsigned int globject =0;
	};

	///--------------------------------------------------------------------------------------------------------------------------
	class GPUTexture2D {
	public:
		//ctors and operators
		GPUTexture2D();
		//create a 2d texture from filename (take only the first max_channels channels), with N mipmap levels (-1 = full tree, NOTE: 1= base level)
		GPUTexture2D(const std::string& filename, unsigned int max_channels, bool mipmaps = true, int mipmaps_max_level = -1, bool flipy = true);
		//create a 2d texture from structure (no data), with N mipmap levels (-1 = full tree, NOTE: 1= base level)
		GPUTexture2D(GLint internal_format, unsigned int data_width, unsigned int data_height, bool mipmaps = true, int mipmaps_max_level = -1);
		//create a 2d texture from structure and base level data, with N mipmap levels (-1 = full tree, NOTE: 1= base level)
		GPUTexture2D(GLint internal_format, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data, bool mipmaps = true, int mipmaps_max_level = -1);
		//disable explicit copies, use clone
		GPUTexture2D(const GPUTexture2D&) = delete;
		GPUTexture2D& operator=(const GPUTexture2D&) = delete;
		//but allow move semantics
		GPUTexture2D(GPUTexture2D&&);
		GPUTexture2D& operator=(GPUTexture2D&&);
		~GPUTexture2D();

		//clone
		GPUTexture2D clone();

		//swap
		void swap(GPUTexture2D& other);

		//create a 2d texture from filename(take only the first max_channels channels), with N mipmap levels (-1 = full tree, NOTE: 1= base level)
		void load(const std::string& filename, unsigned int max_channels, bool mipmaps = true, int mipmaps_max_level = -1, bool flipy = true);
		//create a 2d texture from structure (no data), with N mipmap levels (-1 = full tree, NOTE: 1= base level)
		void create(GLint internal_format, unsigned int data_width, unsigned int data_height, bool mipmaps = true, int mipmaps_max_level = -1);
		//create a 2d texture from structure and base level data, with N mipmap levels (-1 = full tree, NOTE: 1= base level)
		void create(GLint internal_format, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data, bool mipmaps = true, int mipmaps_max_level = -1);
		//resize texture and create N mipmap levels (-1 = full tree, NOTE: 1= base level)
		void resize(unsigned int width, unsigned int height, bool mipmaps = true, int mipmaps_max_level = -1);
		
		//updates the base level and update the entire existing mipmap tree? (no mipmaps are allocated here)
		void update(GLenum data_format, GLenum data_type, const GLvoid* data, bool update_mipmaps);
		//update a specific level in the mipmap tree (should be between 0 and max_level)
		void update(unsigned int level, unsigned int offset_x, unsigned int offset_y, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data);
		//update the entire mipmap tree (no mipmaps are allocated here)
		void updateMipmaps();
		//read from a mipmap to the cpu
		void readToCPU(unsigned int level, GLenum outdata_format, GLenum outdata_type, GLsizei outdata_buffer_size, void* outdata) const;

		//usage
		void bindAsTexture(unsigned int texture_unit) const;
		void bindAsImage(unsigned int image_unit, GLenum access, unsigned int level, GLenum format) const;

		//access
		GLint getInternalFormat() const;
		bool getMipmapsExist() const;
		//1 = only the base level exists => there are no mipmaps
		unsigned int getMipmapsMaxLevel() const;
		unsigned int getWidth() const;
		unsigned int getHeight() const;
		unsigned int getGLobject() const;

		//memory (not exact!)
		size_t getMemoryBase() const;
		size_t getMemoryWithMipmaps() const;

	private:
		unsigned int width = 0;
		unsigned int height = 0;
		size_t memory_base = 0;
		size_t memory_with_mipmaps = 0;
		GLint internal_format = 0;
		GLuint globject =0;
		bool mipmaps = false;
		unsigned int mipmaps_max_level = 0;
	};
}