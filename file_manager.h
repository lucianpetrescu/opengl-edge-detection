///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
#include <string>
#include <vector>
#include "core/gpu_texture.h"
#include "dependencies/lap_image/lap_image.hpp"

namespace edge{

	struct FileOutput {
		unsigned int width = 0;
		unsigned int height = 0;
		unsigned int channels = 0;
		unsigned char* data = nullptr;
		GPUTexture2D* gpudata;
		std::string filename = "";
	};

	class FileManager{
	public:
		FileManager();
		FileManager(const std::string& asset_path);
		FileManager(const FileManager&) = delete;
		FileManager(FileManager&&) = delete;
		FileManager& operator=(const FileManager&) = delete;
		FileManager& operator=(FileManager&&) = delete;
		~FileManager() = default;

		//asset path
		void setAssetPath(const std::string& path);
		const std::string& getAssetPath() const;

		//file cycling
		void setFile(std::string filename);
		void cycleFileNext(std::string from = "");
		void cycleFilePrev(std::string from = "");

		//individual access
		const std::string& getFilename() const;
		unsigned int getWidth() const;
		unsigned int getHeight() const;
		unsigned int getNumChannels() const;
		const unsigned char* getData() const;
		const GPUTexture2D* getDataGPU() const;

		//bulk access
		const FileOutput& getFileOutput() const;

	private:
		const std::string internalNextFile(const std::string& fromfile, bool next);
		void internalLoad(const std::string& file);
		std::string asset_path = "";
		lap::Image image;
		GPUTexture2D texture;
		FileOutput output;
	};
}
