///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
// module needs OpenGL and wic provides it
#include "dependencies/lap_wic/lap_wic.hpp"
#include "core/gpu_texture.h"
#include "core/gpu_shader.h"

namespace edge{

	class GPUEdgeDetector {
	public:
		
		//operators
		enum class Operator { RobertsCross2x2, Sobel3x3, Sobel5x5, Scharr3x3, Prewitt3x3, Prewitt5x5, Kirsch3x3, Kirsch5x5, FreiChen3x3, Line3x3, Log3x3, Log5x5};

		//ctor, dtor
		GPUEdgeDetector();
		~GPUEdgeDetector();
		
		//set input
		void setInput(GLuint texid, int num_channels);
		void setInput(GPUTexture2D* input, int num_channels);
		
		// limitations:
		//   blur_kernel_sigma > 0
		//   blur_kernel_size is an odd unsigned int. if 1 -> no blur
		void computeEdge(float gaussianblur_kernel_sigma, unsigned int gaussianblur_kernel_size, Operator edge_op, float threshold_upper, float threshold_lower, bool non_maximum_suppression, unsigned int hysterezis_iterations, unsigned int thinning_iterations);
		// limitations:
		//   blur_kernel_sigma > 0
		//   blur_kernel_size is an odd unsigned int. if 1 -> no blur
		void computeFlux(float gaussianblur_kernel_sigma, unsigned int gaussianblur_kernel_size, Operator edge_op, float threshold_upper);

		GPUTexture2D* getOutputMagnitude();
		GPUTexture2D* getOutputAngle();
		GLuint getOutputMagnitudeGL();
		GLuint getOutputAngleGL();
		const std::string getOperatorName(Operator edge_op);

		//useful for reloading and debugging shaders
		void debugReloadShaders();
	private:
		struct {
			int num_channels = 3;
			int width = 1 ;
			int height = 1;
			GLuint texid = 0;
		}input;
		GPUTextureSampler sampler_near, sampler_bilinear;

		GPUShader shader_blur;
		GPUShader shader_edge;
		GPUTexture2D gpu_tex1;
		GPUTexture2D gpu_tex2;
		GPUTexture2D gpu_tex3;
	};
}
