@echo off
rem -----------------------------------------------------------------------------------------
rem this pseudo-makefile uses Microsfot Visual Studio 2015. If this IDE is unavailable then the
rem MinGW-enabled makefile can be used. This requires a working 64bit, cpp11 enabled MinGW.
rem -----------------------------------------------------------------------------------------
setlocal

rem set the path to include devenv
if not defined DevEnvDir (
    call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
)

rem determine project name
for %%* in (.) do set projname=%%~nx*

rem targets
:Start
if "%~1" == "debug" goto DebugTarget
if "%~1" == "release" goto ReleaseTarget
if "%~1" == "all" goto AllTarget
if "%~1" == "clean" goto CleanTarget
if "%~1" == "help" goto HelpTarget
if "%~1" == "install" goto InstallTarget
goto AllTarget

:DebugTarget
	echo -------------- Building %projname% in Debug mode ----------------
	devenv "vstudio/edge_detection.sln" /Build Debug /nologo
goto End
	
:ReleaseTarget
	echo -------------- Building %projname% in Release mode --------------
	devenv "vstudio/edge_detection.sln" /Build Release /nologo
goto End

:AllTarget
	echo -------------- Building %projname% in Debug mode ----------------
	devenv "vstudio/edge_detection.sln" /Build Debug /nologo
	echo -------------- Building %projname% in Release mode --------------
	devenv "vstudio/edge_detection.sln" /Build Release /nologo
goto End

:CleanTarget
	echo -------------- Cleaning %projname% ------------------------------
	devenv "vstudio/edge_detection.sln" /Clean Debug /nologo
	devenv "vstudio/edge_detection.sln" /Clean Release /nologo
	if exist bin\Debug ( rd /s /q bin\Debug & echo rd /s /q bin\Debug )
	if exist bin\Release ( rd /s /q bin\Release & echo rd /s /q bin\Release )
	if exist vstudio\.vs ( rd /s /q vstudio\.vs & echo rd /s /q vstudio\.vs )
	if exist bin\*.iobj ( del bin\*.iobj & echo del bin\*.iobj )
	if exist bin\*.ipdb ( del bin\*.ipdb & echo del bin\*.ipdb )
	if exist bin\*.pdb ( del bin\*.pdb & echo del bin\*.pdb )
	if exist vstudio\*.sdf ( del vstudio\*.sdf & echo del vstudio\*.sdf )
	if exist vstudio\*.suo ( del vstudio\*.suo & echo del vstudio\*.suo )
	if exist vstudio\*.db ( del vstudio\*.db & echo del vstudio\*.db )
	if exist vstudio\*.opendb ( del vstudio\*.opendb & echo del vstudio\*.opendb )
goto End

:HelpTarget
	@echo --- %projname% Makefile  ---
	@echo use -make build- or -make all- to build both debug and release executables
	@echo use -make debug- to build only in debug mode
	@echo use -make release- to build only in release mode
	@echo use -make clean- to clean all visual studio related binaries and additional files (.suo, .sdf, .db, .pdb, etc)
goto End

:InstallTarget
	@echo nothing to install
goto End

:End
endlocal